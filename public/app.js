'use strict';
function addUserFetch(userInfo) {
  // напишите POST-запрос используя метод fetch
  let options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({name: userInfo.name, lastname: userInfo.lastname})
  };

  return fetch(`/users`, options)
          .then((jsonObj)=>{
            if(jsonObj.status > 399) {
              return Promise.reject(jsonObj);
            }
            return jsonObj.json();
          })
          .then((jsonObj)=>{
            return Promise.resolve(jsonObj.id);
          })
          .catch(()=>{
            return Promise.reject(`Error code: ${jsonObj.status}, ${jsonObj.statusText}`);
          })
}

function getUserFetch(id) {
  // напишите GET-запрос используя метод fetch
  return fetch(`/users/${id}`)
  .then(jsonObj=>{
    if(jsonObj.status > 399) {
      return Promise.reject(jsonObj);
    }
    return jsonObj.json();
  })
  .then(obj=>{
    return Promise.resolve({name: obj.name, lastname: obj.lastname});
  })
  .catch((jsonObj)=>{
    return Promise.reject(`Error code: ${jsonObj.status}, ${jsonObj.statusText}`);
  });
}

function addUserXHR(userInfo) {
  // напишите POST-запрос используя XMLHttpRequest
  
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();

    xhr.open('POST', '/users');
    xhr.setRequestHeader('Content-Type','application/json');
    xhr.responseType = 'json';

    xhr.onload = ()=>{
      if(xhr.status < 400) {
        resolve(xhr.response.id);
      }
      else {
        reject(`Error status: ${xhr.status}, ${xhr.statusText}`);
      }
    };

    xhr.onerror = ()=>{
      reject(`Error status: ${xhr.status}, ${xhr.statusText}`);
    };

    xhr.send(JSON.stringify(userInfo));

  });
}

function getUserXHR(id) {
  // напишите GET-запрос используя XMLHttpRequest
  return new Promise((resolve, reject)=>{
    const xhr = new XMLHttpRequest();

    xhr.open('GET', `/users/${id}`);

    xhr.onload = ()=>{
      if(xhr.status < 400) {
        const obj = JSON.parse(xhr.response);
        resolve({name: obj.name, lastname: obj.lastname});
      }
      else {
        reject(`Error status: ${xhr.status}, ${xhr.statusText}`);
      }
    };

    xhr.onerror = ()=>{
      reject(`Error status: ${xhr.status}, ${xhr.statusText}`);
    };

    xhr.send();

  });
}

function checkWork() {  
  /*Fetch*/

  // addUserFetch({ name: "Alice", lastname: "FetchAPI" })
  //   .then(userId => {
  //     console.log(`Был добавлен пользователь с userId ${userId}`);
  //     return getUserFetch(userId);
  //     // return getUserFetch(userId+1);
  //   })
  //   .then(userInfo => {
  //     console.log(`Был получен пользователь:`, userInfo);
  //   })
  //   .catch(error => {
  //     console.error(error);
  //   });

  /*XMLHttpRequest*/

  addUserXHR({ name: "Alice", lastname: "FetchAPI" })
  // addUserXHR('Hello')
  .then(userId=>{
    console.log(`Был добавлен пользователь с userId ${userId}`);
    return getUserXHR(userId);
  })
  .then(userObj=>{
    console.log(`Был получен пользователь: `, userObj);
  })
  .catch(error=>{
    console.error(error);
  });
}

checkWork();